/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoinspector.dao;

import fotoinspector.modelo.Inspeccion;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

/**
 *
 * @author juan.herrera
 */
public class InspeccionDAO extends AbstractDAO{
    
    
    public InspeccionDAO() {
        super();
    }
    
    public boolean crear(Inspeccion inspeccion){
        
        mongoOperation.save(inspeccion);
        
        return inspeccion.getId() != null;
    }
    
    public Inspeccion consultarPorNumeroDeFormulario(String numeroFormulario){
        
        Inspeccion resultado = null;
        
        // query to search user
	Query searchUserQuery = new Query(Criteria.where("numeroFormulario").is(numeroFormulario));
        
        resultado = mongoOperation.findOne(searchUserQuery, Inspeccion.class);
        
        return resultado;
    }
}
