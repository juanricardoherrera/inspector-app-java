/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoinspector.modelo;

import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author juan.herrera
 */
@Document(collection = "inspecciones")
public class Inspeccion {
    
    @Id
    private String id;
    
    private String numeroFormulario;
    
    private String numeroContrato;
    
    private String usuario;
    
    private String fecha;
    
    private List<SoporteDocumental> soportesDocumentales;

    public String getNumeroFormulario() {
        return numeroFormulario;
    }

    public void setNumeroFormulario(String numeroFormulario) {
        this.numeroFormulario = numeroFormulario;
    }

    public String getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<SoporteDocumental> getSoportesDocumentales() {
        return soportesDocumentales;
    }

    public void setSoportesDocumentales(List<SoporteDocumental> soportesDocumentales) {
        this.soportesDocumentales = soportesDocumentales;
    }
    
    
}
