package fotoinspector.bpm.core.app.servicios.negocio;

import fotoinspector.bpm.core.app.servicios.util.ClassesMap;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import fotoinspector.bpm.core.app.servicios.util.XMLParser;
import fotoinspector.bpm.core.servicios.entrypoint.SORFacade;
import fotoinspector.bpm.core.util.ErrorManager;
import fotoinspector.comunes.json.JacksonMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import fotoinspector.comunes.entrypoint.SORRequest;
import fotoinspector.comunes.entrypoint.SORResponse;

@Controller
@RequestMapping("/SORFacade")
public class SORFacadeRest {

    /**
     * Encargado de reportar logs de errores en la consola del servidor de
     * aplicaciones
     */
    private final static Logger LOGGER = LoggerFactory.getLogger(SORFacadeRest.class);
    /**
     * Mapa de mensajes de error
     */
    private final ErrorManager error = ErrorManager.getInstance();
    /**
     * Punto de entrada a la aplicacion SOR Java
     */
    @Autowired
    private SORFacade sorFacade;
    /**
     * Ofrece funcionalidades para procesar objetos en formato JSON
     */
    @Autowired
    private JacksonMapper jacksonMapper;

    /**
     * Operacion que actua como fachada para determinar los servicios a invocar
     * de acuerdo con la operacion especificada en el request
     *
     * @param request objeto que especifica la operacion a invocar con sus
     * respectivos parametros
     * @return SORResponse objeto con la informacion de respuesta de la
     * invocacion del request
     * @throws Exception cuando la operacion especificada falla
     */
    @RequestMapping(value = "/sorRequest", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    @ResponseBody

    public String sorRequest(@RequestBody String request) throws Exception {

        SORResponse sorResponse;
        String resultado;
        SORRequest sorRequest = getRequestFromJSON(request);

        try {
            LOGGER.info("Request solicitud: " + request);
            sorResponse = sorFacade.sorRequest(sorRequest);

        } catch (Exception e) {
            String code = "02";
            String errorMessage = error.getErrorMessage(code);
            LOGGER.error("**** " + e.getMessage() + " **** " + errorMessage);
            e.printStackTrace();
            sorResponse = new SORResponse();
            sorResponse.setCode(code);
            sorResponse.setDescription(errorMessage);
            sorResponse.setResponse(e.getMessage());
        }
        resultado = jacksonMapper.writeValueAsString(sorResponse);
        return resultado;

    }

    /**
     * transforma el JSON del request al objeto SORRequest con el parametro body
     * tipado. Los tipos admitidos son {Solicitud, ConsultaCatalogos,
     * OperacionesCreditos, String}
     *
     * @param request cadena que contiene el request en formato JSON
     * @return SORRequest objeto correspondiente al JSON recibido. Null en caso
     * de no encontrar un tipo body valido
     */
    private SORRequest getRequestFromJSON(String request) {

        SORRequest sorRequest = null;
        try {
            int indiceInicial = request.indexOf(":") + 2;
            int indiceFinal = request.indexOf(",", indiceInicial) - 1;
            
            String operacion = request.substring(indiceInicial, indiceFinal);
            
            sorRequest = jacksonMapper.readValue(request, jacksonMapper.constructCollectionType(SORRequest.class, ClassesMap.getClass(operacion)));
            
            return sorRequest;
        } catch (ClassNotFoundException e) {
            LOGGER.error( e.getMessage() );
        } catch (IOException e) {
            LOGGER.error( e.getMessage() );
        }

        return sorRequest;
    }
}
