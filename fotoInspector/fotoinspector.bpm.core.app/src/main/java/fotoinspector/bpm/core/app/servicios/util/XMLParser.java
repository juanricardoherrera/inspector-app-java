
package fotoinspector.bpm.core.app.servicios.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class XMLParser {
    
    public static String getTextContent(String xsd) { 
        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            
            File temp = File.createTempFile("doc", "xsd");
            // Delete temp file when program exits.
            temp.deleteOnExit();

            // Write to temp file
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(temp), "UTF8"));
            out.write(xsd);
            out.close();
            
            Document doc = docBuilder.parse (temp); 
            return doc.getDocumentElement().getTextContent();
        } catch (SAXException ex) {
           
        } catch (IOException ex) {
          
            ex.printStackTrace();
        } catch (ParserConfigurationException ex) {
           
        }
        return "";
    }
    
    public static String response(String result) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><p:result xmlns:p=\"http://MediacionSOR/c\" xmlns:ns0=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xsi:type=\"xsd:string\">"+result+"</p:result>";
    }
}
