/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fotoinspector.bpm.core.app.servicios.util;

import fotoinspector.modelo.Inspeccion;
import java.util.HashMap;


/**
 * Clase singletón que representa un mapa con el nombre de la operación y la
 * clase asociada
 * @author juan.herrera
 */
public class ClassesMap {
    private static ClassesMap _class;
    private HashMap<String, Class> classesMap;
    
    private ClassesMap() {
        classesMap = new HashMap<String, Class>();
    }
    
    /**
     * Método que permite el acceso a la instacia de la clase singleton
     * @return instancia singleton
     */
    private static ClassesMap getInstance(){
        if(_class == null){
            _class = new ClassesMap();
            initMap();
        }
        return _class;
    }
    
    /**
     * Método que obtiene el mapa de nombres y Classes
     * @param keyClass llave definida para la clase
     * @return el Class correspondiente a la llave
     */
    public static Class getClass(String keyClass){
        return getInstance().classesMap.get(keyClass);
    }
    
    /**
     * Inserta un Class con su llave asociada al mapa
     * @param keyClass llave de la clase
     * @param clase Class relacionado
     */
    public static void insertClass(String keyClass, Class clase){
        getInstance().classesMap.put(keyClass, clase);
    }
    
    /**
     * Inicializa el mapa de clases asociados
     * a la operación
     */
    private static void initMap(){
        insertClass("CRINSP", Inspeccion.class);
        insertClass("COINSP", String.class);
    }
}
