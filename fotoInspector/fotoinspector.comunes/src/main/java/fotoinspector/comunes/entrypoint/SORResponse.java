/*
 * Copyright (c) Pragma S.A. 2015. All rights reserved. 
 *
 * Contributors:
 *    David Duran <david.duran@pragma.com.co> - initial API and implementation and/or initial documentation
 */
package fotoinspector.comunes.entrypoint;

import java.io.Serializable;

/**
 *
 * @author David Duran <david.duran@pragma.com.co>
 */
public class SORResponse implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * Codigo del resultado de la invoacion del request. Los valores son tomados del
     * archivo FotoInspectorCommonErrors.properties
     */
    private String code;
    
    /**
     * Descrpicion del resultado de la invoacion del request. Tomado del
     * archivo FotoInspectorCommonErrors.properties
     */
    private String description;
    
    /**
     * Cadena que contiene el JSON del objeto que devuelve la invocacion exitosa
     * del request
     */
    private String response;

    /**
     * Obtiene el codigo del resultado de la operacion
     * @return el codigo
     */
    public String getCode() {
        return code;
    }

    /**
     * Cambia el codigo del resultado de la operacion
     * @param code el nuevo codigo
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Obtiene la descripcion del resultado de la operacion
     * @return la descripcion
     */
    public String getDescription() {
        return description;
    }

    /**
     * Cambia la descripcion del resultado de la operacion
     * @param description la nueva descripcion
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Obtiene el JSON del objeto que retorna la operacion
     * @return el objeto de respuesta en formato JSON
     */
    public String getResponse() {
        return response;
    }

    /**
     * Cambia el objeto que retorna la operacion
     * @param response El nuevo objeto en formato JSON
     */
    public void setResponse(String response) {
        this.response = response;
    }       
}
