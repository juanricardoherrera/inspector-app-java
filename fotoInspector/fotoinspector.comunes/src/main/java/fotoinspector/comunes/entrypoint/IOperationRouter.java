
package fotoinspector.comunes.entrypoint;

/**
 * Interfaz que define el método que direcciona las operaciones por componente
 * @author jairo.valencia
 */
public interface IOperationRouter {
    
    /**
     * Método que determina la operación que debe consumir el componente que lo
     * implementa
     * @param request objeto que especifica la operacion a invocar con sus
     * respectivos parámetros
     * @return SORResponse resultado de la operación invocada
     */
    public SORResponse operationRouter(SORRequest request) throws Exception;
}
