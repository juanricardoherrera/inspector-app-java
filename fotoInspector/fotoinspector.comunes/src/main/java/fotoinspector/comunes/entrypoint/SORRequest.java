/*
 * Copyright (c) Pragma S.A. 2015. All rights reserved. 
 *
 * Contributors:
 *    David Duran <david.duran@pragma.com.co> - initial API and implementation and/or initial documentation
 */
package fotoinspector.comunes.entrypoint;

import java.io.Serializable;

/**
 * Clase que indica la operacion (operation) del SOR que se va a consumir con sus 
 * respectivos parametros de entrada (body)
 * @author David Duran <david.duran@pragma.com.co>
 */
public class SORRequest<T> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    /**
     * Cadena que contiene la operacion a invocar en el request
     */
    private String operation;
    
    /**
     * Objeto que contiene la informacion necesaria para
     * invocar la operacion especificada
     */
    private T body;
    
    /**
     * Cadena que contiene el componente al que pertenece la operación
     */
    private String component;

    /**
     * Obtiene la operacion especificada en el request
     * @return la operation
     */
    public String getOperation() {
        return operation;
    }

    /**
     * Cambia la operacion que va a ejecutar el request
     * @param operation la nueva operacion
     */
    public void setOperation(String operation) {
        this.operation = operation;
    }

    /**
     * Obtiene el objeto con la informacion enviada en el request
     * @return los parametros en formato JSON
     */
    public T getBody() {
        return body;
    }

    /**
     * Cambia el objeto de informacion del request
     * @param body el nuevo objeto de informacion
     */
    public void setBody(T body) {
        this.body = body;
    }

    /**
     * Obtiene el componente de la operación del request
     * @return el nombre del componente
     */
    public String getComponent() {
        return component;
    }

    /**
     * Cambia el componente de la operación del request
     * @param component el nuevo nombre del componente
     */
    public void setComponent(String component) {
        this.component = component;
    }
    
    
}
