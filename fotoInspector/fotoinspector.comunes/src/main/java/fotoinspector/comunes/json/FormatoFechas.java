
package fotoinspector.comunes.json;

import java.text.SimpleDateFormat;

public class FormatoFechas {

    public static SimpleDateFormat obtenerFormatoFechaJSONToTw() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss'Z'");
        return sdf;
    }
}
