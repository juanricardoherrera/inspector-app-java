
package fotoinspector.comunes.json;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import java.text.DateFormat;
import java.util.Date;

/**
 * Clase que genera la representación de un json en un objeto java
 * @author jairo.valencia
 */
public class JacksonMapper extends ObjectMapper {

    private ObjectMapper objectMapper;
    
    public JacksonMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
        
        DateFormat df = FormatoFechas.obtenerFormatoFechaJSONToTw();
        setDateFormat(df);
        
        SimpleModule module = new SimpleModule();
        module.addSerializer(Date.class, new CustomDateSerializer());
        registerModule(module);
        super.registerModule(module);
        objectMapper.registerModule(module);
        objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    }
    
    public JavaType constructCollectionType (Class<?> wrapper, Class<?> inner) throws ClassNotFoundException {
        return objectMapper.getTypeFactory().constructParametricType(Class.forName(wrapper.getName()), Class.forName(inner.getName()));
    }
}
