
package fotoinspector.comunes.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Clase que a partir de una fecha dada genera su equivalente en json
 * @author jairo.valencia
 */
public class CustomDateSerializer extends JsonSerializer<Date> {
    private static final SimpleDateFormat formatter = 
      new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    public CustomDateSerializer() {
        super();
    }

    @Override
    public void serialize(Date t, JsonGenerator jg, SerializerProvider sp) throws IOException, JsonProcessingException {
        jg.writeString(formatter.format(t));
    }
}
