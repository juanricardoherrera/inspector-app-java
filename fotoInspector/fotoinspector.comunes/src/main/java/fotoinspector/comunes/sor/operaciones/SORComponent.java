
package fotoinspector.comunes.sor.operaciones;

/**
 * Define las componentes que expone la aplicacion SOR Java. Las componentes son
 * nombradas de la forma COMP_<NOMBRE>.
 * @author Jairo Valencia <jairo.valencia@pragma.com.co>
 */
public class SORComponent {
    
    public static final String COMP_INSPECCION   = "COMP_INSPECCION";
}
