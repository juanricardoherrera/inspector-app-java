
package fotoinspector.comunes.sor.operaciones;

/**
 * Define las operaciones que expone la aplicacion SOR Java. Las operaciones son
 * nombradas de la forma <NOMBRE_COMPONENTE>_<OPERACION>, donde
 * @author David Duran <david.duran@pragma.com.co>
 */
public class SOROperation {
    
    public static final String CREAR_INSPECCION   = "CRINSP";
    
    public static final String CONSULTAR_INSPECCION   = "COINSP";
}
