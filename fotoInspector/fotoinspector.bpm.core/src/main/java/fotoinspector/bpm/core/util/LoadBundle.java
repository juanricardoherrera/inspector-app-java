
package fotoinspector.bpm.core.util;

import java.util.Locale;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class LoadBundle.
 *
 * @author juan.herrera
 */
public class LoadBundle {
    
    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(LoadBundle.class);
    

	/** The resources. */
	private ResourceBundle resources;

	/**
	 * Contructor de la clase.
	 */
	public LoadBundle() {

	}

	/**
	 * Contructor de la clase con parametros.
	 *
	 * @param archivo the archivo
	 */
	public LoadBundle(String archivo) {
		try {
			resources = ResourceBundle.getBundle(archivo, Locale.getDefault());
		} catch (Exception e) {
			LOGGER.info("Clase: " + e.getClass());
			LOGGER.info("No se puede cargar el archivo: " + archivo);
		}
	}

	/**
	 * Carga el archivo de propiedades.
	 * @param archivo - Debe incluir el nombre del paquete y el nombre del archivo son extencion.
	 */
	public void cargarArchivo(String archivo) {

		try {
			resources = ResourceBundle.getBundle(archivo, Locale.getDefault());
		} catch (Exception e) {
			LOGGER.info("Clase: " + e.getClass());
			LOGGER.info("No se puede cargar el archivo: " + archivo);
		}
	}

	/**
	 * Retorna la propiedad que le pertenece a la llave entregada por parametro.
	 *
	 * @param llave the llave
	 * @return String
	 */
	public String getProperty(String llave) {

		String propiedad = null;
		try {
			propiedad = resources.getString(llave);
		} catch (Exception e) {
			LOGGER.info("Clase: " + e.getClass());
			LOGGER.info("No se puede encontrar la llave: " + llave);
		}

		return propiedad;
	}


}
