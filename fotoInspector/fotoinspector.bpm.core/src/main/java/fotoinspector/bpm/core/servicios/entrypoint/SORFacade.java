
package fotoinspector.bpm.core.servicios.entrypoint;

import fotoinspector.comunes.entrypoint.SORRequest;
import fotoinspector.comunes.entrypoint.SORResponse;


public interface SORFacade {
    
    /**
     * Operacion que actua como fachada para determinar los servicios a invocar
     * de acuerdo con la operacion especificada en el request
     * @param request objeto que especifica la operacion a invocar con sus
     * respectivos parametros
     * @return SORResponse objeto con la informacion de respuesta de la invocacion
     * del request
     * @throws Exception cuando la operacion especificada falla
     */
    public SORResponse sorRequest(SORRequest request) throws Exception;
}
