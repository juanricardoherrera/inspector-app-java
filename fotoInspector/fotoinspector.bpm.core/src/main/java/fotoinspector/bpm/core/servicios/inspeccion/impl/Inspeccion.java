
package fotoinspector.bpm.core.servicios.inspeccion.impl;

import fotoinspector.bpm.core.servicios.inspeccion.IInspeccion;
import fotoinspector.comunes.sor.operaciones.SOROperation;
import fotoinspector.comunes.entrypoint.SORRequest;
import fotoinspector.comunes.entrypoint.SORResponse;
import fotoinspector.comunes.json.JacksonMapper;
import fotoinspector.dao.InspeccionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Inspeccion implements IInspeccion{

    @Autowired
    private JacksonMapper jacksonMapper;
    
    @Autowired
    InspeccionDAO InspeccionDAO;
    
    @Override
    public SORResponse operationRouter(SORRequest request) throws Exception{
        SORResponse sorResponse = new SORResponse();
        sorResponse.setCode("01");
        sorResponse.setDescription("Operación exitosa");
            
        if(SOROperation.CREAR_INSPECCION.equals(request.getOperation())){
            
            sorResponse.setResponse(""+InspeccionDAO.crear((fotoinspector.modelo.Inspeccion)request.getBody()));
            
        }
        if(SOROperation.CONSULTAR_INSPECCION.equals(request.getOperation())){
            
            sorResponse.setResponse("" + "" + jacksonMapper.writeValueAsString(InspeccionDAO.consultarPorNumeroDeFormulario((String)request.getBody())));
            
        }
        
        return sorResponse;
    }

    
}
