
package fotoinspector.bpm.core.util;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ErrorManager {
    
    private static final String PATH_PROPIEDADES = "FotoInspectorCommonErrors";
    private LoadBundle mensajes;
    private static ErrorManager errorManager = null;

    protected ErrorManager() {
        try {
            mensajes = new LoadBundle(PATH_PROPIEDADES);            
        } catch (Exception ex) {
            Logger.getLogger(ErrorManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Retorna el mensaje asociado a un código de error
     *
     * @param key código del error
     * @return El mensaje asociado y si no se encuentra asociado a ningún
     * código, retorna el código de error por defecto (Error general)
     */
    public String getErrorMessage(String key) {
        return mensajes.getProperty(key);       
    }

    /**
     * Devuelve la única instancia creada del ErrorManager
     *
     * @return
     */
    public static ErrorManager getInstance() {
        if (errorManager == null) {
            errorManager = new ErrorManager();
        }
        return errorManager;
    }

}
