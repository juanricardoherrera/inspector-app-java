
package fotoinspector.bpm.core.servicios.entrypoint.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import fotoinspector.bpm.core.servicios.entrypoint.SORFacade;
import fotoinspector.bpm.core.servicios.inspeccion.IInspeccion;
import fotoinspector.comunes.json.JacksonMapper;
import fotoinspector.comunes.sor.operaciones.SORComponent;
import fotoinspector.comunes.entrypoint.SORRequest;
import fotoinspector.comunes.entrypoint.SORResponse;

@Service
public class SORFacadeImpl implements SORFacade {


    @Autowired
    private JacksonMapper jacksonMapper;
    
    @Autowired
    private IInspeccion inspeccionService;

    /**
     * Operacion que actua como fachada para determinar los servicios a invocar
     * de acuerdo con la operacion especificada en el request
     *
     * @param request objeto que especifica la operacion a invocar con sus
     * respectivos parametros
     * @return SORResponse objeto con la informacion de respuesta de la
     * invocacion del request
     * @throws Exception cuando la operacion especificada falla
     */
    @Override
    public SORResponse sorRequest(SORRequest request) throws Exception {
        String component = request.getComponent();
        SORResponse sorResponse = new SORResponse();
        sorResponse.setCode("01");
        sorResponse.setDescription("Operación exitosa");
        
        if (SORComponent.COMP_INSPECCION.equals(component)) {
            sorResponse = inspeccionService.operationRouter(request);
        }

        return sorResponse;
    }

}
